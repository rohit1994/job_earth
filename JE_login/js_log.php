<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>#Job Earth</title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="css/reset.css">

    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
<link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>

        <link rel="stylesheet" href="css/style.css">

    
    
    
  </head>

  <body>

    
<!-- Mixins-->
<!-- Login Part-->
<div class="pen-title">
  <h1>Job Earth</h1>
</div>

<div class="container">
  <div class="card"></div>
  <div class="card">
    <h1 class="title">Job Seeker Login</h1>
     <form  action="" method="post" autocomplete="on"> 
      <div class="input-container">
        <input type="text" id="username" name="username" required="required"/>
        <label for="Username">Username</label>
        <div class="bar"></div>
      </div>
      <div class="input-container">
        <input type="password" id="password" name="password"  required="required"/>
        <label for="Password">Password</label>
        <div class="bar"></div>
      </div>
      <div class="button-container">
        <button type="submit" name="js_log" id="js_log"><span>Go</span></button>
      </div>




      <?php
include("db.php");

if(isset($_POST['js_log']))
{
// username and password sent from Form

$myemail=$_POST['username'];
$mypassword=$_POST['password'];

$sql="SELECT js_id FROM job_seeker WHERE js_email='$myemail' and js_password='$mypassword'";
$result=mysql_query($sql);
$co=mysql_num_rows($result);



// If result matched $myusername and $mypassword, table row must be 1 row
if($co==1)
{ SESSION_START();

$_SESSION['xy']=$myemail;

header("location:test.php");
}
else
{?>

<script>
    alert("Username or password is incorrect!!!");
</script>
<?php }
}
?>
     
      
    </form>
  </div>



<!-- Registration part-->
  <div class="card alt">
    <div class="toggle"></div>
    <h1 class="title" style="margin-bottom:20px"> Job Seeker Register
      <div class="close"></div>
    </h1>
    <form  action="#" method="post" autocomplete="on" enctype="multipart/form-data">
      <div class="input-container">
        <input type="text" id="usernamesignup" name="usernamesignup" required="required"/>
        <label for="Username">Username</label>
        <div class="bar"></div>
      </div>
      <div class="input-container">
        <input type="email" id="emailsignup" name="emailsignup" required="required"/>
        <label for="Email">Email</label>
        <div class="bar"></div>
      </div>
      <div class="input-container">
        <input type="password" id="passwordsignup" name="passwordsignup" required="required"/>
        <label for="Password">Password</label>
        <div class="bar"></div>

      </div>
      <div class="input-container">
      
        <div class="inputWrapper">
    <input class="fileInput" type="file" name="up" id="up"/>
    <h1 style="text-align:center;margin-top:-11%">Profile pic</h1>

</div>
       
       
          
      </div>    
      <div class="button-container" style="padding-top:3%;style=padding-bottom:-1%;">
        <button type="submit" name="js_reg" id="js_reg"><span>Next</span></button>
      </div>




<?php
include("db.php");
if(isset($_POST['js_reg']))
{

  $name=$_POST['usernamesignup'];
  $mail=$_POST['emailsignup'];
  $pass=$_POST['passwordsignup'];
 
  
      $sql="SELECT js_id FROM job_seeker WHERE js_email='$mail'";
$result=mysql_query($sql);
$co=mysql_num_rows($result);



// If result matched $myusername and $mypassword, table row must be 1 row
if($co==1)
{ 
?>
<script>
    alert("Account Already exist please login");
</script>
<?php


}
else
{
    $uploaddir='file_upload/';
    $uploadfile=$uploaddir.basename($_FILES['up']['name']);
    echo $uploadfile;
     if(move_uploaded_file($_FILES['up']['tmp_name'],$uploadfile))
    {
        $z="insert into job_seeker values('','$name','$mail','$pass','$uploadfile')";
        mysql_query($z); ?>

<script>
    alert("Registration Successfull! login to continue");
</script>
<?php
}




}
}
?>


    </form>
  </div>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="js/index.js"></script>

    
    
    
  </body>
</html>
