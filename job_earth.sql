-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1build0.15.04.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 31, 2016 at 10:12 AM
-- Server version: 5.6.28-0ubuntu0.15.04.1
-- PHP Version: 5.6.4-4ubuntu6.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `job_earth`
--

-- --------------------------------------------------------

--
-- Table structure for table `employer`
--

CREATE TABLE IF NOT EXISTS `employer` (
`emp_id` int(100) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `emp_email` varchar(100) NOT NULL,
  `emp_password` varchar(100) NOT NULL,
  `emp_pic` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employer`
--

INSERT INTO `employer` (`emp_id`, `emp_name`, `emp_email`, `emp_password`, `emp_pic`) VALUES
(1, 'Rohit Yadav', 'rohit@gmail.com', '2481994', 'file_upload/deh.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker`
--

CREATE TABLE IF NOT EXISTS `job_seeker` (
`js_id` int(100) NOT NULL,
  `js_name` varchar(100) NOT NULL,
  `js_email` varchar(100) NOT NULL,
  `js_password` varchar(100) NOT NULL,
  `js_pic` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_seeker`
--

INSERT INTO `job_seeker` (`js_id`, `js_name`, `js_email`, `js_password`, `js_pic`) VALUES
(1, 'Rohit Yadav', 'rohit@gmail.com', '2481994', 'file_upload/deh.jpg'),
(2, 'shubham mittal', 'shubham@gmail.com', 'scscsc', 'file_upload/3.jpg'),
(3, 'Rohit Yadav', 'surabhi@gmail.com', '2481994', 'file_upload/Gagrism10.jpg'),
(4, 'shubham mittal', 'shubham11@gmail.com', '2481994', 'file_upload/3.jpg'),
(5, 'Rohit Yadav', 'rohit11@gmail.com', '2481994', 'file_upload/chalan.pdf'),
(6, 'Rohit Yadav', 'rohitcheck@gmail.com', '2481994', 'file_upload/imageedit_2_3439003963.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employer`
--
ALTER TABLE `employer`
 ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `job_seeker`
--
ALTER TABLE `job_seeker`
 ADD PRIMARY KEY (`js_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employer`
--
ALTER TABLE `employer`
MODIFY `emp_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `job_seeker`
--
ALTER TABLE `job_seeker`
MODIFY `js_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
